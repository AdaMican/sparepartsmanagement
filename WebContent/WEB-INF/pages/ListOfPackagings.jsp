<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Packaging management</title>
</head>
<body>
	<h1>The list of packagings for spare parts:</h1>
	<c:if test="${not empty message}">
		<c:out value="${message}" />
	</c:if>



	<table style="border: 1px solid;">
		<c:forEach items="${valueFromList}" var="packaging">
			<tr>
				<td style="border: 1px solid; background-color:lightgreen">  Packaging PN:</td>
				<td style="border: 1px solid; "><c:out value="${packaging.getPN_packaging()}" /></td>
				<td style="border: 1px solid; background-color:lightgreen">  Packaging PN description:</td>
				<td  style="border: 1px solid; "><c:out value="${packaging.getPackaging_description()}" /></td>
				<td style="border: 1px solid; background-color:lightgreen">  Packaging quantity for product:</td>
				<td style="border: 1px solid; "><c:out value="${packaging.getQty_for_product()}" /></td>
				
				<td>
					<form
						action="http://localhost:8080/SparePartsManagement/Packaging/removePackaging"
						method="get">
						<input value="${packaging.getPN_packaging()}" name="PN_packaging" hidden> <input
							type="submit" value="Delete">
					</form>
				</td>
				<td>
					<form
						action="http://localhost:8080/SparePartsManagement/Packaging/updatePackagingById"
						method="get">
						<input value="${packaging.getIdpackaging()}" name="idpackaging" hidden>
						<input type="submit" value="Update">
					</form></td></tr>
</c:forEach></table>

<h1>Add new packaging in spare parts data base:</h1>
	<c:if test="${not empty addMessage}">
		<c:out value="${addMessage}" />
	</c:if>
	<form
		action="http://localhost:8080/SparePartsManagement/Packaging/addPackaging"
		method=POST>
		<br> New packaging PN:<input type="text" name="PN_packaging"
			placeholder="PN_packaging"> <br> New packaging description:<input
			type="packaging_description" name="packaging_description"
			placeholder="packaging_description"> <br> New packaging quantity: <input
			type="qty_for_product" name="qty_for_product"
			placeholder="qty_for_productn"> <br> Product for which the packaging is added: <input
			type="idproducts" name="idproducts"
			placeholder="idproducts">
			 <br><input type="submit"
			value="enter"></input>
	</form>
</body>
</html>