<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Order management</title>
</head>
<body>
	<h1>The list of orders for spare parts:</h1>
	<c:if test="${not empty message}">
		<c:out value="${message}" />
	</c:if>



	<table style="border: 1px solid;">
		<c:forEach items="${valueFromList}" var="Order">
			<tr>
			<td>The order number:</td>
				<td><c:out value="${order.getIdorder()}" /></td>
				<td>The creator of the order:</td>
				<td><c:out value="${order.getOrder_creator()}" /></td>
				<td>The status of the order transfer on AS01 packaging line:</td>
				<td><c:out value="${order.isStatus_transfer_AS01()}" /></td>
				<td>The status of packaged order from AS01 packaging line:</td>
				<td><c:out value="${order.isStatus_pack_AS01()}" /></td>
				<td>The status of order delivery:</td>
				<td><c:out value="${order.isStatus_delivery()}" /></td>
				</tr>
				</c:forEach>
</body>
</html>