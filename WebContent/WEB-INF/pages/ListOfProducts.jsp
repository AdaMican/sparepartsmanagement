<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product management</title>
</head>
<body>
	<h1>The list of product for spare parts:</h1>
	<c:if test="${not empty message}">
		<c:out value="${message}" />
	</c:if>



	<table style="border: 1px solid;">
		<c:forEach items="${valueFromList}" var="product">
			<tr>
				<td>Product PN:</td>
				<td><c:out value="${product.getPN()}" /></td>
				<td>Product PN description:</td>
				<td><c:out value="${product.getPN_description()}" /></td>
				<td>
					<form
						action="http://localhost:8080/SparePartsManagement/Product/removeProduct"
						method="get">
						<input value="${product.getPN()}" name="PN" hidden> <input
							type="submit" value="Delete">
					</form>
				</td>
				<td>
					<form
						action="http://localhost:8080/SparePartsManagement/Product/updateProductById/"
						method="get">
						<input value="${product.getIdproducts()}" name="idproducts" hidden>
						<input type="submit" value="Update">
					</form></td>
			</tr>
		</c:forEach>
	</table>

	<h1>Add new PN's in spare parts data base:</h1>
	<c:if test="${not empty addMessage}">
		<c:out value="${addMessage}" />
	</c:if>
	<form
		action="http://localhost:8080/SparePartsManagement/Product/addProduct"
		method=POST>
		<br> New product's PN:<input type="text" name="PN"
			placeholder="PN"> <br> New product's description:<input
			type="PN_description" name="PN_description"
			placeholder="PN_description"> <br> <input type="submit"
			value="enter"></input>
	</form>
</body>
</html>