
package com.sda.SparePartsManagement.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.SparePartsManagement.Service.ProductService;
import com.sda.SparePartsManagement.Service.ProductServiceImpl;
import com.sda.SparePartsManagement.model.Product;


@Controller
@RequestMapping("/Product")
public class ProductController {

	@Autowired(required = true)
	private ProductService productService;
	
	
	@RequestMapping(value = "/getAllProducts", method = RequestMethod.GET)
	public String getAllProducts(ModelMap list) {

		list.addAttribute("valueFromList", productService.getAllProducts());
		System.out.println("This is the implementation for get all products ");
		return "ListOfProducts";
	}
	
	//URL test: http://localhost:8080/SparePartsManagement/Product/addProductHibernate

	@RequestMapping(value = "/addProduct", method = RequestMethod.GET)
	public String addProduct() {
		return "AddNewProduct";
	}

	@RequestMapping(value = "/addProduct", method = RequestMethod.POST)
	public String addProduct( @RequestParam("PN") String PN,
			@RequestParam("PN_description") String PN_description,  ModelMap list) {
		productService.addProduct(new Product( PN, PN_description));
		list.addAttribute("valueFromList", productService.getAllProducts());
		list.addAttribute("message","The product" + PN + " has beed added" );
		System.out.println("The product" + PN + " has beed added");
		return "ListOfProducts";
	}
	
	@RequestMapping(value = "/removeProduct", method = RequestMethod.GET)
	public String removeProduct(@RequestParam("PN") String PN, ModelMap list) {
		productService.removeProductByPN(PN);
		
		list.addAttribute("valueFromList", productService.getAllProducts());
		list.addAttribute("message","The product" + PN + " has beed deleted" );
		System.out.println("The product" + PN + " has beed deleted");
		return "ListOfProducts";
	}
	
	@RequestMapping(value = "/updateProductById", method = RequestMethod.GET)
	public String updateProductById(@RequestParam("idproducts") int idproducts, ModelMap list) {
		Product infoProduct=productService.getProductById(idproducts);
		list.addAttribute("product", infoProduct);
		return "UpdateProductForm";
	}
	
	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
	public String updateProduct(@ModelAttribute("product") Product product,  ModelMap list) {
		productService.updateProduct(product);
		list.addAttribute("valueFromList", productService.getAllProducts());
		list.addAttribute("message","The product" + product.getPN() + " has beed updated" );
		return "ListOfProducts";
	}


}