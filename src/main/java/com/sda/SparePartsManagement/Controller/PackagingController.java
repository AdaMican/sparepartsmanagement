package com.sda.SparePartsManagement.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.SparePartsManagement.Service.PackagingService;
import com.sda.SparePartsManagement.Service.ProductService;
import com.sda.SparePartsManagement.model.Packaging;
import com.sda.SparePartsManagement.model.Product;

@Controller
@RequestMapping ("/Packaging")
public class PackagingController {
	
	@Autowired(required = true)
	private PackagingService packagingService;
	@Autowired(required = true)
	private ProductService productService;
	
	@RequestMapping(value = "/getAllPackagings", method = RequestMethod.GET)
	public String getAllPackagings(ModelMap list) {
		list.addAttribute("valueFromList", packagingService.getAllPackagings());
		list.addAttribute("productList", productService.getAllProducts());
		System.out.println("All packaging were returned");
		return "ListOfPackagings";
		
	}
	
	@RequestMapping(value = "/removePackaging", method = RequestMethod.GET)
	public String removePackaging(@RequestParam("PN_packaging") String PN_packaging, ModelMap list) {
		packagingService.removePackagingByPN(PN_packaging);
		list.addAttribute("ListOfPackagings", packagingService.getAllPackagings());
		list.addAttribute("message","The packaging " + PN_packaging + " has beed deleted" );
		System.out.println("The packaging" + PN_packaging + " has beed deleted");
		return "ListOfPackagings";
		
	}
	
	@RequestMapping(value = "/updatePackagingById", method = RequestMethod.GET)
	public String updatePackagingById(@RequestParam("idpackaging") int idpackaging, ModelMap list) {
		Packaging infoPackaging=packagingService.getPackagingById(idpackaging);
		list.addAttribute("packaging", infoPackaging);
		return "UpdatePackagingForm";
	}
	
	@RequestMapping(value = "/updatePackaging", method = RequestMethod.POST)
	public String updatePackaging(@ModelAttribute("packaging") Packaging pack,  ModelMap list) {
		packagingService.updatePackaging(pack);
		list.addAttribute("valueFromList", packagingService.getAllPackagings());
		list.addAttribute("message","The packaging" + pack.getPN_packaging() + " has beed updated" );
		return "ListOfPackagings";
	}
	@RequestMapping(value = "/addPackaging", method = RequestMethod.GET)
	public String addProduct(ModelMap list) {
		list.addAttribute("listProducts", productService.getAllProducts());
		return "AddNewPackaging";
	}

	@RequestMapping(value = "/addPackaging", method = RequestMethod.POST)
	public String addPackaging( @RequestParam("PN_packaging") String PN_packaging,
			@RequestParam("packaging_description") String packaging_description, 
			@RequestParam("qty_for_product") double qty_for_product, @RequestParam("idproducts") int idproducts, ModelMap list) {
		Product p=new Product();
		p.setIdproducts(idproducts);
		Packaging packaging = new Packaging(p, PN_packaging, packaging_description, qty_for_product);
		packagingService.addNewPackaging(packaging);
			
		list.addAttribute("valueFromList", packagingService.getAllPackagings());
		list.addAttribute("message","The packaging" + PN_packaging + " has beed added" );
		System.out.println("The packaging" + PN_packaging+ " has beed added");
		return "ListOfPackagings";
	}
	
	

}
