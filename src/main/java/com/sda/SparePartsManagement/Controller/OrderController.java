package com.sda.SparePartsManagement.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sda.SparePartsManagement.DAO.OrderDao;
import com.sda.SparePartsManagement.Service.OrderService;

@Controller
@RequestMapping("/Order")
public class OrderController {
	
	@Autowired (required=true)
	private OrderDao orderDao;
	@Autowired (required=true)
	private OrderService orderService;
	
	@RequestMapping (value="/getAllOrders", method=RequestMethod.GET)
	public String getAllOrders(ModelMap list) {
		list.addAttribute("valueFromList", orderService.getAllOrders());
		System.out.println("All orders were listed");
		return "ListOfOrders";
	}

}
