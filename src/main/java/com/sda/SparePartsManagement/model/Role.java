package com.sda.SparePartsManagement.model;

public class Role {
	
	private int idrole;
	private String role;
	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Role(int idrole, String role) {
		super();
		this.idrole = idrole;
		this.role = role;
	}
	public int getIdrole() {
		return idrole;
	}
	public void setIdrole(int idrole) {
		this.idrole = idrole;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idrole;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (idrole != other.idrole)
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Role [idrole=" + idrole + ", role=" + role + "]";
	}
	
	

}