package com.sda.SparePartsManagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="order_stat")
public class Order {
	
	@Id
	@Column
	private int idorder;
	@Column
	private String order_creator;
	@Column
	private boolean status_transfer_AS01;
	@Column
	private boolean  status_pack_AS01;
	@Column
	private boolean  status_delivery;
	
	public Order() {
		super();
		
	}
	public Order(int idorder, String order_creator, boolean status_transfer_AS01, boolean status_pack_AS01,
			boolean status_delivery) {
		super();
		this.idorder = idorder;
		this.order_creator = order_creator;
		this.status_transfer_AS01 = status_transfer_AS01;
		this.status_pack_AS01 = status_pack_AS01;
		this.status_delivery = status_delivery;
	}
	public int getIdorder() {
		return idorder;
	}
	public void setIdorder(int idorder) {
		this.idorder = idorder;
	}
	public String getOrder_creator() {
		return order_creator;
	}
	public void setOrder_creator(String order_creator) {
		this.order_creator = order_creator;
	}
	public boolean isStatus_transfer_AS01() {
		return status_transfer_AS01;
	}
	public void setStatus_transfer_AS01(boolean status_transfer_AS01) {
		this.status_transfer_AS01 = status_transfer_AS01;
	}
	public boolean isStatus_pack_AS01() {
		return status_pack_AS01;
	}
	public void setStatus_pack_AS01(boolean status_pack_AS01) {
		this.status_pack_AS01 = status_pack_AS01;
	}
	public boolean isStatus_delivery() {
		return status_delivery;
	}
	public void setStatus_delivery(boolean status_delivery) {
		this.status_delivery = status_delivery;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idorder;
		result = prime * result + ((order_creator == null) ? 0 : order_creator.hashCode());
		result = prime * result + (status_delivery ? 1231 : 1237);
		result = prime * result + (status_pack_AS01 ? 1231 : 1237);
		result = prime * result + (status_transfer_AS01 ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (idorder != other.idorder)
			return false;
		if (order_creator == null) {
			if (other.order_creator != null)
				return false;
		} else if (!order_creator.equals(other.order_creator))
			return false;
		if (status_delivery != other.status_delivery)
			return false;
		if (status_pack_AS01 != other.status_pack_AS01)
			return false;
		if (status_transfer_AS01 != other.status_transfer_AS01)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Order [idorder=" + idorder + ", order_creator=" + order_creator + ", status_transfer_AS01="
				+ status_transfer_AS01 + ", status_pack_AS01=" + status_pack_AS01 + ", status_delivery="
				+ status_delivery + "]";
	}
	
	
	
	
	
}