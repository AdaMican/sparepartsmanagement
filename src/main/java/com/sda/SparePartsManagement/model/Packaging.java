package com.sda.SparePartsManagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="packaging")
public class Packaging {

	@Id
	@Column
	private int idpackaging;
	@Column
	private String PN_packaging;
	@Column
	private String packaging_description;
	@Column
	private double qty_for_product;
	@ManyToOne
    @JoinColumn(name="idproducts", nullable=false)
	
    private Product product;
	public Packaging() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Packaging(Product p, String pN_packaging, String packaging_description, double qty_for_product) {
		super();
		this.product = p;
		this.PN_packaging = pN_packaging;
		this.packaging_description = packaging_description;
		this.qty_for_product = qty_for_product;
	}

	public int getIdpackaging() {
		return idpackaging;
	}

	public void setIdpackaging(int idpackaging) {
		this.idpackaging = idpackaging;
	}

	public String getPN_packaging() {
		return PN_packaging;
	}

	public void setPN_packaging(String pN_packaging) {
		PN_packaging = pN_packaging;
	}

	public String getPackaging_description() {
		return packaging_description;
	}

	public void setPackaging_description(String packaging_description) {
		this.packaging_description = packaging_description;
	}

	public double getQty_for_product() {
		return qty_for_product;
	}

	public void setQty_for_product(int qty_for_product) {
		this.qty_for_product = qty_for_product;
	}

	

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public void setQty_for_product(double qty_for_product) {
		this.qty_for_product = qty_for_product;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((PN_packaging == null) ? 0 : PN_packaging.hashCode());
		result = prime * result + idpackaging;
		result = prime * result + ((packaging_description == null) ? 0 : packaging_description.hashCode());
		long temp;
		temp = Double.doubleToLongBits(qty_for_product);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Packaging other = (Packaging) obj;
		if (PN_packaging == null) {
			if (other.PN_packaging != null)
				return false;
		} else if (!PN_packaging.equals(other.PN_packaging))
			return false;
		if (idpackaging != other.idpackaging)
			return false;
		if (packaging_description == null) {
			if (other.packaging_description != null)
				return false;
		} else if (!packaging_description.equals(other.packaging_description))
			return false;
		if (Double.doubleToLongBits(qty_for_product) != Double.doubleToLongBits(other.qty_for_product))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Packaging [idpackaging=" + idpackaging + ", PN_packaging=" + PN_packaging + ", packaging_description="
				+ packaging_description + ", qty_for_product=" + qty_for_product + "]";
	}

	
}
