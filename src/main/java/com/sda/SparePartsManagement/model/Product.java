package com.sda.SparePartsManagement.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="products")
public class Product {
	
	@Id
	@Column
	private int idproducts;
	@Column
	private String PN;
	@Column
	private String PN_description;
	@OneToMany(mappedBy="product")
    private Set<Packaging> packagings;
	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product( String pN, String pN_description) {
		super();
				PN = pN;
		PN_description = pN_description;
	}
	public int getIdproducts() {
		return idproducts;
	}
	public void setIdproducts(int idproducts) {
		this.idproducts = idproducts;
	}
	public String getPN() {
		return PN;
	}
	public void setPN(String pN) {
		PN = pN;
	}
	public String getPN_description() {
		return PN_description;
	}
	public void setPN_description(String pN_description) {
		PN_description = pN_description;
	}
	public Set<Packaging> getPackagings() {
		return packagings;
	}
	public void setPackagings(Set<Packaging> packagings) {
		this.packagings = packagings;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((PN == null) ? 0 : PN.hashCode());
		result = prime * result + ((PN_description == null) ? 0 : PN_description.hashCode());
		result = prime * result + idproducts;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (PN == null) {
			if (other.PN != null)
				return false;
		} else if (!PN.equals(other.PN))
			return false;
		if (PN_description == null) {
			if (other.PN_description != null)
				return false;
		} else if (!PN_description.equals(other.PN_description))
			return false;
		if (idproducts != other.idproducts)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Product [idproducts=" + idproducts + ", PN=" + PN + ", PN_description=" + PN_description + "]";
	}
	
	
}