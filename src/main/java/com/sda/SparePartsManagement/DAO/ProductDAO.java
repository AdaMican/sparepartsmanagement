
package com.sda.SparePartsManagement.DAO;

import java.util.List;

import com.sda.SparePartsManagement.model.Product;

public interface ProductDAO{
	public void addProduct(Product p);
	public void removeProduct(Product p);
	public List <Product> getAllProducts();
	public Product getProductById(int idproducts);
	public void removeProductByPN(String PN);
	public Product updateProductById(int idproducts);
	public void updateProduct(Product p);
}