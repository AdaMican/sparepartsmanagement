package com.sda.SparePartsManagement.DAO;

import java.util.List;

import com.sda.SparePartsManagement.model.Order;



public interface OrderDao {
	
	public Order getOrderById(int idorder);
	public Order getOrderByCreator(String order_creator);
	public void getOrderStatusTransferAS01(boolean status_transfer_AS01);
	public void getOrderStatusPackAS01(boolean  status_pack_AS01);
	public void getOrderStatusDelivery(boolean status_delivery);
	public List<Order> getAllOrders();

}
