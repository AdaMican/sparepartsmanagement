package com.sda.SparePartsManagement.DAO;

import java.util.List;

import com.sda.SparePartsManagement.model.User;



public interface UserDAO {
	
	public void getUserByName(String first_name);
public void addUser(User u);
	
	public void removeUser(User u);
	
	public List<User> getAllUsers();

	public void removeUserByName(String userName);
	
	public void updateUserByName(String userName);

	
	

}
