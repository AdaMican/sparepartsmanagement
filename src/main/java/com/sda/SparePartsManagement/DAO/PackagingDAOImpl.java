package com.sda.SparePartsManagement.DAO;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.SparePartsManagement.model.Packaging;
import com.sda.SparePartsManagement.model.Product;

@Repository
public class PackagingDAOImpl implements PackagingDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public void addNewPackaging(Packaging pack) {
		Session sesion = sessionFactory.getCurrentSession();
		sesion.save(pack);

	}

	public void removePackagingByPN(String PN_packaging) {
		Session sesion = sessionFactory.getCurrentSession();
		String hql = "delete from Packaging where PN_packaging = :PN_packaging";
        Query q = sesion.createQuery(hql).setParameter("PN_packaging", PN_packaging);
        q.executeUpdate();

	}

	public List<Packaging> getAllPackagings() {
		Session sesion = sessionFactory.getCurrentSession();
		List<Packaging> packs = sesion.createQuery("from Packaging").list();

		for (Packaging p : packs) {
			System.out.println(p);
		}

		return packs;

	}

	public Packaging getPackagingById(int idpackaging) {
		
		 return (Packaging) sessionFactory.getCurrentSession().get(Packaging.class, idpackaging);
	}

	public void updatePackaging(Packaging pack) {
		Session sesion = sessionFactory.getCurrentSession();
		sesion.update(pack);

	}

	
}