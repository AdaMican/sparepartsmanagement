package com.sda.SparePartsManagement.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.SparePartsManagement.model.Order;


@Repository
public class OrderDAOImpl implements OrderDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Order getOrderById(int idorder) {
		return (Order) sessionFactory.getCurrentSession().get(Order.class, idorder);

	}

	public Order getOrderByCreator(String order_creator) {
		return (Order) sessionFactory.getCurrentSession().get(Order.class, order_creator);

	}

	public void getOrderStatusTransferAS01(boolean status_transfer_AS01) {
		sessionFactory.getCurrentSession().get(Order.class, status_transfer_AS01);

	}

	public void getOrderStatusPackAS01(boolean status_pack_AS01) {
		sessionFactory.getCurrentSession().get(Order.class, status_pack_AS01);

	}

	public void getOrderStatusDelivery(boolean status_delivery) {
		sessionFactory.getCurrentSession().get(Order.class, status_delivery);

	}

	public List<Order> getAllOrders() {
		Session sesion = sessionFactory.getCurrentSession();
		List<Order> ord = sesion.createQuery("from order_stat").list();

		for (Order o : ord) {
			System.out.println(o);
		}

		return ord;

	}

}
