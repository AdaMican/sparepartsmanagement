package com.sda.SparePartsManagement.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.SparePartsManagement.model.Product;


@Repository
public class ProductDAOImpl implements ProductDAO {
	@Autowired
	private SessionFactory sessionFactory;

	

	public void addProduct(Product p) {
		sessionFactory.getCurrentSession().save(p);
						
	}

	
	public void removeProduct(Product p) {
		Session sesion = sessionFactory.openSession();
			sesion.delete(p);
						
	}

	public List<Product> getAllProducts() {
		Session sesion = sessionFactory.openSession();
		
		List<Product> products = sesion.createQuery("from Product").list();
		for (Product p : products) {
			System.out.println(p);
		}
		sesion.close();
		return products;
	}

	public void removeProductByPN(String PN) {
		Session sesion = sessionFactory.getCurrentSession();
		String hql = "delete from Product where PN = :PN";
        Query q = sesion.createQuery(hql).setParameter("PN", PN);
        q.executeUpdate();
	}


	public Product updateProductById(int idproducts) {
		return (Product) sessionFactory.getCurrentSession().get(Product.class, idproducts);
		
	}
	


	public void updateProduct(Product p) {
		Session sesion = sessionFactory.getCurrentSession();
		sesion.update(p);
		
	}


	

	public Product getProductById(int idproducts) {
		return (Product) sessionFactory.getCurrentSession().get(Product.class, idproducts);
		
	}

}
