package com.sda.SparePartsManagement.DAO;

import java.util.List;

import com.sda.SparePartsManagement.model.Packaging;
import com.sda.SparePartsManagement.model.Product;

public interface PackagingDAO {
	
	public void addNewPackaging(Packaging pack);
	public void removePackagingByPN(String PN_packaging);
	public List<Packaging> getAllPackagings();
	public Packaging getPackagingById(int idpackaging);
	public void updatePackaging(Packaging pack);

}
