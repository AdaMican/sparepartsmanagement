package com.sda.SparePartsManagement.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.SparePartsManagement.DAO.OrderDao;
import com.sda.SparePartsManagement.model.Order;
@Service
@Transactional
public class OrderServiceImpl implements OrderService{
	
	@Autowired
	OrderDao orderDao;

	public Order getOrderById(int idorder) {
			return orderDao.getOrderById(idorder);
	}

	public Order getOrderByCreator(String order_creator) {
		return orderDao.getOrderByCreator(order_creator);
	}

	public void getOrderStatusTransferAS01(boolean status_transfer_AS01) {
		orderDao.getOrderStatusTransferAS01(status_transfer_AS01);
		
	}

	public void getOrderStatusPackAS01(boolean status_pack_AS01) {
		orderDao.getOrderStatusPackAS01(status_pack_AS01);
		
	}

	public void getOrderStatusDelivery(boolean status_delivery) {
		orderDao.getOrderStatusDelivery(status_delivery);
		
	}

	public List<Order> getAllOrders() {
			return orderDao.getAllOrders();
			
	}

}
