package com.sda.SparePartsManagement.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.SparePartsManagement.DAO.PackagingDAO;
import com.sda.SparePartsManagement.model.Packaging;
@Service
@Transactional
public class PackagingServiceImpl implements PackagingService{

	
	@Autowired
	PackagingDAO packagingDao;
	
	public void addNewPackaging(Packaging pack) {
		packagingDao.addNewPackaging(pack);
		
	}

	public void removePackagingByPN(String PN_packaging) {
		packagingDao.removePackagingByPN(PN_packaging);
		
	}

	public List<Packaging> getAllPackagings() {
			return packagingDao.getAllPackagings();
	}

	public Packaging getPackagingById(int idpackaging) {
			return packagingDao.getPackagingById(idpackaging);
	}

	public void updatePackaging(Packaging pack) {
		packagingDao.updatePackaging(pack);
		
	}

}
