package com.sda.SparePartsManagement.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.SparePartsManagement.DAO.ProductDAO;
import com.sda.SparePartsManagement.model.Product;

@Service
@Transactional
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductDAO productDao;
	
	public ProductDAO getProductDao() {
		return productDao;
	}

	public void setProductDao(ProductDAO productDao) {
		this.productDao = productDao;
	}

	public void addProduct(Product p) {
		productDao.addProduct(p);
		
	}

	public void removeProduct(Product p) {
		productDao.removeProduct(p);
		
	}

	public List<Product> getAllProducts() {
				return productDao.getAllProducts();
	}

	public void removeProductByPN(String PN) {
		productDao.removeProductByPN(PN);
		
	}

	public Product updateProductById(int idproducts) {
		 return productDao.updateProductById(idproducts);
		
	}

	public void updateProduct(Product p) {
		productDao.updateProduct(p);
		
	}

	

	public Product getProductById(int idproducts) {
		return productDao.getProductById(idproducts);
	}

	

}
