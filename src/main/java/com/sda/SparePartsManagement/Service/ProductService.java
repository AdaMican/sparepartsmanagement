
package com.sda.SparePartsManagement.Service;

import java.util.List;

import com.sda.SparePartsManagement.model.Product;

public interface ProductService{
	
	
	public void addProduct(Product p);
	public void removeProduct(Product p);
	public List <Product> getAllProducts();
	public void removeProductByPN(String PN);
	public Product updateProductById(int idproducts);
	public void updateProduct(Product p);
	public Product getProductById(int idproducts);
	
}