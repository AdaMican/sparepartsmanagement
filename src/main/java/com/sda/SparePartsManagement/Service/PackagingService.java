package com.sda.SparePartsManagement.Service;

import java.util.List;

import com.sda.SparePartsManagement.model.Packaging;

public interface PackagingService {
	
	public void addNewPackaging(Packaging pack);
	public void removePackagingByPN(String PN_packaging);
	public List<Packaging> getAllPackagings();
	public Packaging getPackagingById(int idpackaging);
	public void updatePackaging(Packaging pack);


}
